package controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;


import java.io.*;
import java.net.*;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private ServerTCP serverTCP;
    private ServerUDP serverUDP;
    private boolean isStart = false;

    @FXML
    TextField tfPortNumber;
    @FXML
    TextArea taLogMessage;
    @FXML
    Button btnStart;
    @FXML
    Button btnReset;
    @FXML
    Button btnExit;
    @FXML
    RadioButton rbTCP;
    @FXML
    RadioButton rbUDP;
    @FXML
    Label lblStatus;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Initialize Server
        serverTCP = new ServerTCP();
        serverUDP = new ServerUDP();

        //Button Disable Setup
        btnStart.setDisable(true);
        btnReset.setDisable(true);
        rbTCP.setSelected(true);

        // Add TextField EventHandler
        tfPortNumber.setOnKeyReleased(event -> onKeyReleasePortNumTextField());
    }

    private void onKeyReleasePortNumTextField() {
        if (!tfPortNumber.getText().isEmpty()) {
            btnStart.setDisable(false);
            btnStart.setFocusTraversable(true);
            btnReset.setDisable(false);
        } else {
            btnStart.setDisable(true);
            btnReset.setDisable(true);
        }
    }

    public void onStartClicked() {
        isStart = !isStart;
        if (isStart) {
            btnStart.setText("Stop");
            btnReset.setDisable(true);
            btnExit.setDisable(true);
            tfPortNumber.setDisable(true);
            rbTCP.setDisable(true);
            rbUDP.setDisable(true);

            // Thread Action Here
            int portNumber = 8080;
            try {
                portNumber = Integer.parseInt(tfPortNumber.getText());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            // Decide Protocol to Run
            if (rbTCP.isSelected()) {
                serverTCP = new ServerTCP();
                serverTCP.setPortNumber(portNumber);
                serverTCP.start();
                taLogMessage.appendText("TCP Server is Running\n");
                taLogMessage.appendText("-----------------------------------------------------------------\n");
            } else {
                serverUDP = new ServerUDP();
                serverUDP.setPortNumber(portNumber);
                serverUDP.start();
                taLogMessage.appendText("UDP Server is Running\n");
                taLogMessage.appendText("-----------------------------------------------------------------\n");
            }
            lblStatus.setText("RUNNING");
            lblStatus.setStyle("-fx-text-fill: green");

        } else {
            btnStart.setText("Start");
            btnReset.setDisable(false);
            btnStart.setDisable(false);
            btnExit.setDisable(false);
            tfPortNumber.setDisable(false);
            rbTCP.setDisable(false);
            rbUDP.setDisable(false);
            taLogMessage.clear();

            // Thread Action Here
            if (rbTCP.isSelected()) {
                serverTCP.stop();
            } else {
                serverUDP.stop();
            }
            lblStatus.setText("STOPPED");
            lblStatus.setStyle("-fx-text-fill: red");
        }
    }

    public void onResetClicked() {
        taLogMessage.clear();
        rbTCP.setSelected(true);
        btnStart.setDisable(true);
        btnReset.setDisable(true);
        tfPortNumber.clear();
        tfPortNumber.requestFocus();
        lblStatus.setText("NOT RUNNING");
        lblStatus.setStyle("-fx-text-fill: black");
    }

    public void onExitClicked() {
        Stage window = (Stage) btnExit.getParent().getScene().getWindow();
        window.close();
    }

    private class ServerTCP extends Thread {
        int portNumber = 8080;
        boolean isThreadStart = true;
        @Override
        public void run() {
            try {
                ServerSocket welcomeSocket = new ServerSocket(portNumber);
                while (isThreadStart) {
                    Socket connectionSocket = welcomeSocket.accept();

                    BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                    String fromClient = inFromClient.readLine();

                    //Append Text to Text Area
                    Platform.runLater(() -> taLogMessage.appendText("The number is: " + fromClient + " from client " + connectionSocket.getInetAddress() + " on port " + connectionSocket.getPort() + "\n"));

                    DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());

//                    Thread.sleep(5000);

                    String reply = "SUCCESS";
                    outToClient.writeUTF(reply);
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        void setPortNumber(int portNumber) {
            this.portNumber = portNumber;
        }
    }

    private class ServerUDP extends Thread {
        int portNumber = 8080;
        boolean isThreadStart = true;
        @Override
        public void run() {
            try {
                DatagramSocket serverSocket = new DatagramSocket(portNumber);
                byte[] receivedData = new byte[1024];
                byte[] sendData;
                while (isThreadStart) {
                    DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);
                    serverSocket.receive(receivedPacket);

                    String sentence = new String(receivedPacket.getData());
                    String number = "";
                    for (int i = 0; i < sentence.length() && sentence.charAt(i) >= '0' && sentence.charAt(i) <= '9'; i++) {
                        number += sentence.charAt(i);
                    }
                    InetAddress IPAddress = receivedPacket.getAddress();
                    int port = receivedPacket.getPort();

                    //Append Text to Text Area
                    String finalNumber = number;
                    Platform.runLater(() -> taLogMessage.appendText("The number is: " + finalNumber + " from client " + IPAddress + " on port " + port + "\n"));

                    String reply = "SUCCESS";
                    sendData = reply.getBytes();

                    Thread.sleep(5000);

                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                    serverSocket.send(sendPacket);
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        void setPortNumber(int portNumber) {
            this.portNumber = portNumber;
        }
    }
}