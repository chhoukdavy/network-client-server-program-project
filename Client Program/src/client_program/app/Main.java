package client_program.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        try{
            Parent root = FXMLLoader.load(getClass().getResource(".." +
                    File.separator + "view" +
                    File.separator + "MainView.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("stylesheets.css").toExternalForm());
            stage.setScene(scene);
            stage.setTitle("Client Program");
            int STAGE_H = 405;
            stage.setMaxHeight(STAGE_H);
            stage.setMinHeight(STAGE_H);
            int STAGE_W = 600;
            stage.setMaxWidth(STAGE_W);
            stage.setMinWidth(STAGE_W);
            stage.show();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}

