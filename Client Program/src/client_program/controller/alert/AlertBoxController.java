package client_program.controller.alert;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class AlertBoxController extends AnchorPane {

    private Stage window;

    @FXML
    public Label lblMessage;

    public AlertBoxController () {
        window = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../view/alert/AlertBoxLayout.fxml"));
            loader.setController(this);
            loader.setRoot(this);
            loader.load();
            Scene scene = new Scene(this);
            window.setScene(scene);
            window.setMaxHeight(180);
            window.setMaxWidth(380);
            window.setMinHeight(180);
            window.setMinWidth(380);
            window.setTitle("Message");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onCloseClicked() {
        window.close();
    }
    public void display() {
        window.show();
    }
    public void setMessageText(String strText) {
        lblMessage.setText(strText);
    }

}
