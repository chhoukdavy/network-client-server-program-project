package client_program.controller.tab;

import client_program.controller.MainController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.ConnectException;
import java.net.URL;
import java.util.ResourceBundle;

public class TabGUIController implements Initializable {
    private MainController mainController = new MainController();
    private ClientThread clientThread;

    @FXML
    TextField tfNumSend;
    @FXML
    TextField tfHostName;
    @FXML
    TextField tfPortNum;
    @FXML
    ToggleGroup tgConnType;
    @FXML
    RadioButton rbTCP;
    @FXML
    RadioButton rbUDP;

    @FXML
    public Label lblserverReply;

    @FXML
    Button btnSend;
    @FXML
    Button btnReset;
    @FXML
    Button btnExit;

    public TabGUIController() {

    }

    @FXML
    public void onSendClicked() {

        btnExit.setDisable(true);
        btnReset.setDisable(true);
        lblserverReply.setText("WAITING...");
        lblserverReply.setStyle("-fx-text-fill: black");

        String strNumber, strHostName, strPortNum, strConnType;
        //Initialize String
        strNumber = tfNumSend.getText();
        strHostName = tfHostName.getText();
        strPortNum = tfPortNum.getText();
        if (rbTCP.isSelected()) {
            strConnType = "tcp";
        } else {
            strConnType = "udp";
        }

        if (clientThread != null) {
            clientThread.interrupt();
            clientThread = null;
        }

        clientThread = new ClientThread(strNumber, strHostName, strPortNum, strConnType);

        if (mainController.validateInput(strNumber, strHostName, strPortNum, strConnType)) {
            //Alert The Message
            mainController.alertDisplay(strNumber, strHostName, strPortNum, strConnType);
            //Start Socket Thread
            clientThread.start();
        } else {
            //Set Server Reply Error Message
            mainController.tabGUIController.lblserverReply.setText("ERROR!");
            mainController.tabGUIController.lblserverReply.setStyle("-fx-text-fill: red");
            mainController.tabConsoleController.lblserverReply.setText("ERROR!");
            mainController.tabConsoleController.lblserverReply.setStyle("-fx-text-fill: red");
        }

        btnExit.setDisable(false);
        btnReset.setDisable(false);
    }

    @FXML
    public void onBtnResetClicked() {
        tfHostName.clear();
        tfNumSend.clear();
        tfPortNum.clear();
        lblserverReply.setText("Server Reply");
        lblserverReply.setStyle("-fx-text-fill: black");
    }

    @FXML
    public void onBtnExitClicked() {
        Stage window = (Stage) btnExit.getParent().getScene().getWindow();
        window.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnSend.setDisable(true);
        btnReset.setDisable(true);

        tfNumSend.setOnKeyReleased(event -> onBtnEnable());
        tfHostName.setOnKeyReleased(event -> onBtnEnable());
        tfPortNum.setOnKeyReleased(event -> onBtnEnable());

        btnExit.setDisable(false);

    }

    private void onBtnEnable() {
        if(!tfNumSend.getText().isEmpty() && !tfHostName.getText().isEmpty() && !tfPortNum.getText().isEmpty()) {
            btnSend.setDisable(false);
        }
        if(!tfNumSend.getText().isEmpty() || !tfHostName.getText().isEmpty() || !tfPortNum.getText().isEmpty()) {
            btnReset.setDisable(false);
        }
    }

    private class ClientThread extends Thread {
        String protocol = "";
        String number = "";
        String hostname = "";
        String portNumber = "";

        ClientThread(String number, String hostname, String portNumber, String protocol) {
            this.protocol = protocol;
            this.portNumber = portNumber;
            this.hostname = hostname;
            this.number = number;
        }

        @Override
        public void run() {
            if (protocol.equalsIgnoreCase("tcp")) {
                mainController.openTCP_operation(hostname, portNumber, number);
            } else {
                mainController.openUDP_operation(hostname, portNumber, number);
            }
        }
    }

    //Mediator
    public void init(MainController mainController) {
        this.mainController = mainController;
    }
}
