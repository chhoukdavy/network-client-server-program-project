package client_program.controller.tab;


import client_program.controller.MainController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class TabConsoleController implements Initializable {

    private MainController main = new MainController();
    private ClientThread clientThread;
    private Logger logger = LoggerFactory.getLogger(MainController.class);

    public TabConsoleController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnSend.setDisable(true);
        btnReset.setDisable(true);

        taConsole.setOnKeyReleased(event -> onButtonEnable());
    }

    private void onButtonEnable() {
        if(!taConsole.getText().isEmpty()) {
            btnSend.setDisable(false);
            btnReset.setDisable(false);
        } else {
            btnSend.setDisable(true);
            btnReset.setDisable(true);
        }
    }

    public void init(MainController mainController) {
        this.main = mainController;
    }

    @FXML
    public TextArea taConsole;
    @FXML
    public Label lblserverReply;

    @FXML
    public Button btnSend;
    @FXML
    public Button btnReset;
    @FXML
    public Button btnExit;

    @FXML
    public void onSendClicked() {
        lblserverReply.setText("WAITING...");
        lblserverReply.setStyle("-fx-text-fill: black");

        String input = taConsole.getText();
        operateInput(input);
    }

    private void operateInput(String input) {

        String strNumToSend = getNumInput(input);
        String strConnType = getConnProtocol(input);
        String strServerAddress = getHostName(input);
        String strPortNumber = getPortNum(input);

        if (clientThread != null) {
            clientThread.interrupt();
            clientThread = null;
        }

        clientThread = new ClientThread(strNumToSend, strServerAddress, strPortNumber, strConnType);

        if (!strNumToSend.equalsIgnoreCase("") && !strConnType.equalsIgnoreCase("") && !strServerAddress.equalsIgnoreCase("") && !strPortNumber.equalsIgnoreCase("")) {
            main.alertDisplay(strNumToSend, strServerAddress, strPortNumber, strConnType);
            //Start Client Thread
            clientThread.start();
        } else {
            main.tabGUIController.lblserverReply.setText("ERROR!");
            main.tabGUIController.lblserverReply.setStyle("-fx-text-fill: red");
            main.tabConsoleController.lblserverReply.setText("ERROR!");
            main.tabConsoleController.lblserverReply.setStyle("-fx-text-fill: red");
        }
    }


    @FXML
    public void onBtnResetClicked() {
        taConsole.clear();
        lblserverReply.setText("Server Reply");
    }

    @FXML
    public void onBtnExitClicked() {
        Stage window = (Stage) btnExit.getParent().getScene().getWindow();
        window.close();
    }

    private String getNumInput(String input) {
        int index = -1;
        for (int i = 0; i < input.length() - 3; i++) {
            if (input.charAt(i) == '-' && input.charAt(i + 1) == 'x' && input.charAt(i + 2) == ' ' && input.charAt(i + 3) == '<') {
                index = i;
            }
        }

        if(index != -1) {
            String numberInput = "";
            for (int j = index + 4;  j < input.length() && input.charAt(j) != '>'; j++) {
                numberInput += input.charAt(j);
            }
            boolean valid = main.validateNumber(numberInput);
            if(!valid) {
                return "";
            }
            return numberInput;
        }

        logger.warn("NO NUMBER INPUT!");
        return "";
    }

    private String getConnProtocol(String input) {
        int index = -1;
        for (int i = 0; i < input.length() - 5; i++) {
            if (input.charAt(i) == '-' && input.charAt(i + 1) == 't' && input.charAt(i + 2) == ' ' && input.charAt(i + 3) == 'u' && input.charAt(i + 4) == 'd' && input.charAt(i + 5) == 'p' ||
                    input.charAt(i) == '-' && input.charAt(i + 1) == 't' && input.charAt(i + 2) == ' ' && input.charAt(i + 3) == 't' && input.charAt(i + 4) == 'c' && input.charAt(i + 5) == 'p') {
                index = i;
            }
        }

        if(index != -1) {
            String protocol = "" + input.charAt(index + 3) + input.charAt(index + 4) + input.charAt(index + 5);
            if(protocol.equalsIgnoreCase("tcp") || protocol.equalsIgnoreCase("udp")) {
                return "" + input.charAt(index + 3) + input.charAt(index + 4) + input.charAt(index + 5);
            } else {
                logger.warn("CONNECTION PROTOCOL INVALID!");
            }
        } else {
            logger.warn("NO VALID CONNECTION PROTOCOL INPUT!");
        }
        return "";
    }

    private String getHostName(String input) {
        int index = -1;
        for (int i = 0; i < input.length() - 1; i++) {
            if (input.charAt(i) == '-' && input.charAt(i + 1) == 's') {
                index = i;
            }
        }

        if(index != -1) {
            String address = "";
            for (int i = index + 3; i < input.length() && input.charAt(i) != ' '; i++) {
                address += input.charAt(i);
            }

            boolean valid = main.validateHostName(address);
            if(!valid) {
                return "";
            }

            return address;
        }

        logger.warn("NO HOSTNAME INPUT!");
        return "";
    }

    private String getPortNum(String input) {
        int index = -1;
        for (int i = 0; i < input.length() - 1; i++) {
            if (input.charAt(i) == '-' && input.charAt(i + 1) == 'p') {
                index = i;
            }
        }

        if(index != -1) {
            String portNum = "";
            for (int i = index + 3; i < input.length() && input.charAt(i) != ' '; i++) {
                portNum += input.charAt(i);
            }

            boolean valid = main.validatePortNumber(portNum);
            if(!valid) {
                logger.warn("PORT NUMBER INVALID!");
                return "";
            }
            return portNum;
        }

        logger.warn("NO PORT NUMBER INPUT!");
        return "";
    }

    private class ClientThread extends Thread {
        String protocol = "";
        String number = "";
        String hostname = "";
        String portNumber = "";

        ClientThread(String number, String hostname, String portNumber, String protocol) {
            this.protocol = protocol;
            this.portNumber = portNumber;
            this.hostname = hostname;
            this.number = number;
        }

        @Override
        public void run() {
            if (protocol.equalsIgnoreCase("tcp")) {
                main.openTCP_operation(hostname, portNumber, number);
            } else {
                main.openUDP_operation(hostname, portNumber, number);
            }
        }
    }
}
