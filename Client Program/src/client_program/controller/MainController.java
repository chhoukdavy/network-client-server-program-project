package client_program.controller;

import client_program.controller.alert.AlertBoxController;
import client_program.controller.tab.TabConsoleController;
import client_program.controller.tab.TabGUIController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private Logger logger = LoggerFactory.getLogger(MainController.class);
    private AlertBoxController alertBoxController = new AlertBoxController();

    @FXML
    Label lblDeveloper;

    @FXML
    public TabGUIController tabGUIController;
    @FXML
    public TabConsoleController tabConsoleController;

    public MainController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Application Started...");
        tabGUIController.init(this);
        tabConsoleController.init(this);
    }

    public void openTCP_operation(String host, String portNum, String numberToSend) {
        try {
            //Open TCP Connection
            Socket clientSocket = new Socket(host, Integer.parseInt(portNum));
            clientSocket.setSoTimeout(3000);

            //Send to Server
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            outToServer.writeBytes(numberToSend + '\n');

            //Receive from Server //Timer starts
            long tStart = System.currentTimeMillis();
            InputStream inFromServer = clientSocket.getInputStream();
            DataInputStream dataInputStream = new DataInputStream(inFromServer);

            String data;
            try {
                data = dataInputStream.readUTF();
                //setLblServerReply
                setLblServerReply(data.equals("SUCCESS"));
                //confirm success
                System.out.println("Server Communication: SUCCESS");
            } catch (Exception exception) {
                setLblServerReply(false);
                System.out.println("Server Communication: RESPONSE TIMEOUT");
            }

            long tEnd = System.currentTimeMillis();
            System.out.println("TIME-lAPSE: " + (tEnd - tStart) + " MILLIS");

            // Close Connection
            clientSocket.close();
        } catch (ConnectException conE) {
            setLblServerReply(false);
            logger.warn("CONNECTION REFUSED!");
        } catch (IOException ignored) {
            logger.warn("OUTPUT FAILED!");
        }
    }

    public void openUDP_operation(String hostName, String portNum, String numberToSend) {
        String response;
        try {
            //Open UDP Connection
            DatagramSocket clientSocket = new DatagramSocket();
            clientSocket.setSoTimeout(3000);
            InetAddress IPAddress = InetAddress.getByName(hostName);

            byte[] sendData, receiveData = new byte[1024];
            sendData = numberToSend.getBytes();
            String handledResponse = "";

            //Connection and Send
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, Integer.parseInt(portNum));
            clientSocket.send(sendPacket);

            //Receive //Timer starts
            long tStart = System.currentTimeMillis();
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            try {
                clientSocket.receive(receivePacket);
                response = new String(receivePacket.getData());
                for (int i = 0; i < response.length() && response.charAt(i) >= 'A' && response.charAt(i) <= 'Z'; i++) {
                    handledResponse += response.charAt(i);
                }
                // Set Server Reply Message
                setLblServerReply(handledResponse.equals("SUCCESS"));
                System.out.println("Server Communication: SUCCESS");
            } catch (Exception exception) {
                setLblServerReply(false);
                System.out.println("Server Communication: TIMEOUT");
            }

            //Timer ends
            long tEnd = System.currentTimeMillis();
            System.out.println("TIME-lAPSE: " + (tEnd - tStart) + " MILLIS");

            //Close Connection
            clientSocket.close();
        } catch (ConnectException conE) {
            setLblServerReply(false);
            logger.warn("CONNECTION REFUSED!");
        } catch (IOException ignored) {
            logger.warn("OUTPUT FAILED!");
        }
    }

    private void setLblServerReply(boolean timeout) {
        if (timeout) {
            Platform.runLater(() -> {
                tabGUIController.lblserverReply.setText("SUCCESS!");
                tabGUIController.lblserverReply.setStyle("-fx-text-fill: green;");
                tabConsoleController.lblserverReply.setText("SUCCESS!");
                tabConsoleController.lblserverReply.setStyle("-fx-text-fill: green;");
            });
        } else {
            Platform.runLater(() -> {
                tabGUIController.lblserverReply.setText("ERROR!");
                tabGUIController.lblserverReply.setStyle("-fx-text-fill: red");
                tabConsoleController.lblserverReply.setText("ERROR!");
                tabConsoleController.lblserverReply.setStyle("-fx-text-fill: red");
            });
        }
    }

    // "Sent number to server IP:port via TCP"
    public void alertDisplay(String number, String hostname, String portNum, String connType) {
        alertBoxController.setMessageText("SENT " + number + " TO SERVER " + hostname + ":" + portNum + " VIA " + connType.toUpperCase());
        alertBoxController.display();
    }

    // validate Input method
    public boolean validateInput(String strNumber, String strHostName, String strPortNumber, String connType) {
        boolean validNum = validateNumber(strNumber);
        boolean validHostName = validateHostName(strHostName);
        boolean validPortNum = validatePortNumber(strPortNumber);
        boolean validConnType = validateConnType(connType);

        return validNum && validHostName && validPortNum && validConnType;
    }

    public boolean validateNumber(String numberInput) {
        if (numberInput.isEmpty()) {
            logger.warn("NUMBER INVALID!");
            return false;
        }

        try {
            int number = Integer.parseInt(numberInput);
            if (number >= 0 && number < Math.pow(2, 31) - 1) {
                return true;
            }
            logger.warn("NUMBER INVALID!");
        } catch (Exception e) {
            logger.warn("NUMBER INVALID!");
            return false;
        }
        return false;
    }

    public boolean validateHostName(String hostname) {
        if (hostname.isEmpty()) {
            logger.warn("HOSTNAME INVALID!");
            return false;
        }

        try {
            InetAddress hostNameAddress = InetAddress.getByName(hostname);
            return true;
        } catch (UnknownHostException e) {
            logger.warn("HOSTNAME INVALID!");
            return false;
        }
    }

    public boolean validatePortNumber(String portNum) {
        if (portNum.isEmpty()) {
            logger.warn("PORT NUMBER INVALID!");
            return false;
        }

        String regex = "[0-9]+";
        if (portNum.matches(regex)) {
            try {
                if (Integer.parseInt(portNum) < 65536 && Integer.parseInt(portNum) > 0) {
                    return true;
                }
                logger.warn("PORT NUMBER INVALID!");
            } catch (Exception e) {
                logger.warn("PORT NUMBER INVALID!");
                return false;
            }
        }
        logger.warn("PORT NUMBER INVALID!");
        return false;
    }

    private boolean validateConnType(String connType) {
        if (connType.equals("tcp") || connType.equals("udp")) {
            return true;
        } else {
            logger.warn("CONNECTION PROTOCOL INVALID!");
            return false;
        }
    }

}
